Select2
=======================================
This is a wrapper around jquery plugin select2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist junati/yii2-select2 "*"
```

or add

```
"junati/yii2-select2": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \junati\select2\AutoloadExample::widget(); ?>```