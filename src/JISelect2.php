<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 28/02/19
 * Time: 12:34 PM
 */

namespace junati\select2;

use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * JISelect2 widget is a Yii2 wrapper for the Select2 jQuery plugin. This input widget is a jQuery based replacement for
 * select boxes. It supports searching, remote data sets, and infinite scrolling of results.
 *
 * @author Vishal Parashar <vishal@junati.com>
 * @since 1.0
 */
class JISelect2 extends InputWidget
{
    public $pluginOptions;
    public $data;

    public function init()
    {
        parent::init();
        if($this->data == null){
            $this->data = [];
        }
    }

    public function run()
    {
        parent::run();
        $this->renderWidget();
    }

    public function renderWidget()
    {
        $this->initPlaceholder();
        $this->registerAssets();

        Html::addCssClass($this->options, 'form-control');
        $input = $this->getInput('dropDownList', true);
        echo $input;

    }

    protected function getInput($type, $list = false)
    {
        if ($this->hasModel()) {
            $input = 'active' . ucfirst($type);
            return $list ?
                Html::$input($this->model, $this->attribute, $this->data, $this->options) :
                Html::$input($this->model, $this->attribute, $this->options);
        }
        $input = $type;
        $checked = false;
        if ($type == 'radio' || $type == 'checkbox') {
            $checked = ArrayHelper::remove($this->options, 'checked', '');
            if (empty($checked) && !empty($this->value)) {
                $checked = ($this->value == 0) ? false : true;
            } elseif (empty($checked)) {
                $checked = false;
            }
        }
        return $list ?
            Html::$input($this->name, $this->value, $this->data, $this->options) :
            (($type == 'checkbox' || $type == 'radio') ?
                Html::$input($this->name, $checked, $this->options) :
                Html::$input($this->name, $this->value, $this->options));
    }

    protected function initPlaceholder()
    {
        $isMultiple = ArrayHelper::getValue($this->options, 'multiple', false);
        if (isset($this->options['prompt']) && !isset($this->pluginOptions['placeholder'])) {
            $this->pluginOptions['placeholder'] = $this->options['prompt'];
            if ($isMultiple) {
                unset($this->options['prompt']);
            }
            return;
        }
        if (isset($this->options['placeholder'])) {
            $this->pluginOptions['placeholder'] = $this->options['placeholder'];
            unset($this->options['placeholder']);
        }
        if (isset($this->pluginOptions['placeholder']) && is_string($this->pluginOptions['placeholder']) && !$isMultiple) {
            $this->options['prompt'] = $this->pluginOptions['placeholder'];
        }
    }

    public function registerAssets()
    {
        $isSelectAll = !isset($this->pluginOptions['ajax']) && isset($this->options['multiple']) ? true : false;
        $this->pluginOptions = Json::htmlEncode($this->pluginOptions);
        $id = $this->options['id'];
        $view = $this->getView();
        JISelect2Asset::register($view);
        $script = "jQuery('#" . $id . "').select2({$this->pluginOptions})";
        $view->registerJs("jQuery.when({$script}).done(initS2Loading('{$id}', '{$isSelectAll}'))");
    }
}