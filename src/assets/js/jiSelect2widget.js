
function initS2Loading(id, isSelectAll) {
    let element = $('#'+id);
    let totalDataLength;
    let selectedDataLength;

    if(isSelectAll){
        element.on('select2:open', function(){
            if(!$("#parent-s2-togall-"+id).length){
                $('.select2-results').prepend('' +
                    '<span id="parent-s2-togall-'+id+'" style="margin-left : 12px; cursor: pointer;">' +
                    '<span id="s2-togall-'+id+'" class="s2-togall-button s2-togall-select">' +
                    '<span id="s2-select-label-'+id+'" style="display: inline; line-height: 1.5; font-size: 0.875rem;">' +
                    '<i class="glyphicon glyphicon-unchecked" style="margin-right: 5px;"></i>Select all' +
                    '</span>' +
                    '<span id="s2-unselect-label-'+id+'" style="display: inline; line-height: 1.5; font-size: 0.875rem;">' +
                    '<i class="glyphicon glyphicon-check" style="margin-right: 5px;"></i>Unselect all' +
                    '</span>' +
                    '</span>' +
                    '</span>' ).css('margin-top', '5px');

                $('#s2-unselect-label-'+id+'').on('click', function(){
                    element.val(null).trigger('change');
                    $('.select2-results__options li').attr('aria-selected', false);
                    $(this).hide();
                    $('#s2-select-label-'+id+'').show();
                    element.select2('close');
                });

                $('#s2-select-label-'+id+'').on('click', function(){
                    let mySelectedArray = [];
                    $('#select2-' + id + '-results').find('[role="treeitem"]').each(function (k,v) {
                        let currentNode = $('#'+v.id);
                        let arrayLength = currentNode.data('select2-id').split("-").length;
                        mySelectedArray.push(currentNode.data('select2-id').split("-")[arrayLength - 1]);
                    });
                    element.val(mySelectedArray).trigger('change');
                    $('.select2-results__options li').attr('aria-selected', true);
                    element.select2('close');
                });
            }

            totalDataLength = $('#' + id + ' option').length;
            selectedDataLength = $("#"+id).val().length;
            if(selectedDataLength < totalDataLength){
                $('#s2-unselect-label-'+id+'').hide();
                $('#s2-select-label-'+id+'').show();
            } else if(selectedDataLength === 0){
                $('#s2-unselect-label-'+id+'').hide();
                $('#s2-select-label-'+id+'').show();
            } else if(selectedDataLength === totalDataLength){
                $('#s2-select-label-'+id+'').hide();
                $('#s2-unselect-label-'+id+'').show();
            }
        });
    }

}